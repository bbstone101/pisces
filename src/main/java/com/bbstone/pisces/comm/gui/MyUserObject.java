package com.bbstone.pisces.comm.gui;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MyUserObject {
    private String filepath;
    private String filename;

}
