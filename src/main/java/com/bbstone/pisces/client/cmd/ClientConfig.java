package com.bbstone.pisces.client.cmd;

import javax.swing.*;

public class ClientConfig {
    private JTextField serverHost;
    private JTextField serverPort;
    private JTextField clientDir;
    private JRadioButton enabledRadioButton;
    private JRadioButton disabledRadioButton;
    private JButton saveButton;
    private JButton cancelButton;
}
